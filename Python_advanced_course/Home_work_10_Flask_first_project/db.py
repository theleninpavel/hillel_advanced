import sqlite3
from typing import List
from datetime import datetime


def get_connection(db_name: str):
    connection = sqlite3.connect(db_name)
    connection.row_factory = sqlite3.Row
    return connection


def init_db(db_name: str):
    with get_connection(db_name) as db_connection:
        cursor = db_connection.cursor()
        cursor.execute("""PRAGMA foreign_keys = ON""")
        cursor.execute(
            """CREATE TABLE IF NOT EXISTS type
            (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                type TEXT UNIQUE NOT NULL
            )
            """)

        cursor.execute(
            """CREATE TABLE IF NOT EXISTS tdl
            (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                activity TEXT NOT NULL,
                type_id INTEGER,
                participants VARCHAR(40) NOT NULL,
                price VARCHAR(5) NOT NULL,
                link VARCHAR(40) DEFAULT NULL,
                key VARCHAR(40) NOT NULL,
                accessibility VARCHAR(5) NOT NULL,
                date VARCHAR(10) NOT NULL,
                FOREIGN KEY (type_id)
                    REFERENCES type (id)
            )
            """)
        cursor.close()
        db_connection.commit()


def reading_from_to_do_list_today(db_name: str) -> List:
    with get_connection(db_name) as db_connection:
        today = datetime.now().strftime("%d-%m-%y")
        cursor = db_connection.cursor()
        result = cursor.execute(
            f"""SELECT t.activity, y.type, t.date
            FROM tdl t
            JOIN type y ON y.id == t.type_id
            WHERE date LIKE '{today}' 
            """)
        temp_list = []
        for item in result:
            temp_list.append(dict(item))
        cursor.close()
        db_connection.commit()
        return temp_list


def reading_from_to_do_list_category(db_name: str, category) -> List:
    with get_connection(db_name) as db_connection:
        cursor = db_connection.cursor()
        result = cursor.execute(
            f"""SELECT t.activity, y.type, t.date
            FROM tdl t
            JOIN type y ON y.id == t.type_id
            WHERE y.type LIKE '{category}' 
            """)
        temp_list = []
        for item in result:
            temp_list.append(dict(item))
        cursor.close()
        db_connection.commit()
        return temp_list


def reading_from_to_do_list_all(db_name: str) -> List:
    with get_connection(db_name) as db_connection:
        cursor = db_connection.cursor()
        result = cursor.execute(
            f"""SELECT t.activity, y.type, t.date
            FROM tdl t
            JOIN type y ON y.id == t.type_id
            """)
        temp_list = []
        for item in result:
            temp_list.append(dict(item))
        cursor.close()
        db_connection.commit()
        return temp_list


def reading_from_type(db_name: str) -> List:
    with get_connection(db_name) as db_connection:
        cursor = db_connection.cursor()
        result = cursor.execute(
            """SELECT type
            FROM type
            """)
        list_of_types = []
        for i in result:
            list_of_types.extend(list(i))
        cursor.close()
        db_connection.commit()
        return list_of_types


def edit_type_table(db_name: str, list_of_types: List,  prmtr: str):
    with get_connection(db_name) as db_connection:
        cursor = db_connection.cursor()
        if prmtr not in list_of_types:
            cursor.execute(f"""INSERT INTO type(type) VALUES ('{prmtr}')""")
        cursor.close()
        db_connection.commit()


def edit_tdl_table(db_name: str, data_from_api):
    print(f"edit_tdl_table: {data_from_api}")
    with get_connection(db_name) as db_connection:
        cursor = db_connection.cursor()
        result = cursor.execute(
            """SELECT id, type
            FROM type
            """)
        list_of_types = []
        for i in result:
            list_of_types.extend(list(i))
        result = cursor.execute(
            """SELECT id, type
            FROM type
            """)
        dict_of_types = {v: k for (k, v) in result}
        cursor.execute(
            """INSERT INTO tdl(activity, type_id, participants, price, link, key, accessibility, date)
               VALUES (?,?,?,?,?,?,?,?)""",
                        [data_from_api['activity'],
                         dict_of_types.get(data_from_api.get('type')),
                         data_from_api['participants'],
                         data_from_api['price'],
                         data_from_api['link'],
                         data_from_api['key'],
                         data_from_api['accessibility'],
                         datetime.now().strftime("%d-%m-%y")])
    cursor.close()
    db_connection.commit()
