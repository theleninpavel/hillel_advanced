from flask_wtf import FlaskForm

from wtforms import SubmitField, SelectField



class SelectForm(FlaskForm):
    list = ["education", "recreational", "social", "diy", "charity", "cooking", "relaxation", "music", "busywork"]
    category = SelectField('All available categories in API', choices=list)
    login = SubmitField('Выбор')


class SelectForm2(FlaskForm):
    deal_type = SelectField('Available types from database', coerce=str)
    login = SubmitField('Выбор')



