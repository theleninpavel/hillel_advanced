from app import app
from db import init_db

if __name__ == '__main__':
    init_db(app.config.get('DB_URL'))
    import routes
    app.run()
