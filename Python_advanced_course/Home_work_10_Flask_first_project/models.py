class UserModel:
    def __init__(self, activity, type: str, participants: str, price: str, key: str, accessibility: str, link=None):
        self.activity = activity
        self.type = type
        self.participants = participants
        self.price = price
        self.key = key
        self.accessibility = accessibility
        self.link = link
