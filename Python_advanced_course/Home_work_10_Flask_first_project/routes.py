

from flask import render_template, request, url_for
from werkzeug.utils import redirect

from app import app
from helpers import random_deal_request
from db import edit_type_table, reading_from_type, edit_tdl_table, reading_from_to_do_list_all, \
    reading_from_to_do_list_today, reading_from_to_do_list_category
from forms import SelectForm, SelectForm2

menu = [{"name": "Main page", "url": "/"},
        {"name": "Activity for today", "url": "date"},
        {"name": "List of activity for particular category", "url": "category"},
        {"name": "Add new activity", "url": "deal"},
        {"name": "Add new type of activity",  "url": "new_type"}]


@app.route('/')
def main():
    title = 'Main page'
    data = reading_from_to_do_list_all(app.config.get('DB_URL'))
    return render_template('index.html', title=title, menu=menu, data=data)


@app.route('/date')
def activity_for_today():
    title = 'Activity for today'
    data = reading_from_to_do_list_today(app.config.get('DB_URL'))
    return render_template('date.html', title=title, menu=menu, data=data)


@app.route('/category', methods=['GET', 'POST'])
def show_particular_category():
    title = 'List of activity for particular category'
    method = request.method
    form = SelectForm2()
    form.deal_type.choices = reading_from_type(app.config.get('DB_URL'))
    if method == "POST":
        keyword_for_request = request.form['deal_type']
        data = reading_from_to_do_list_category(app.config.get('DB_URL'), keyword_for_request)
        return render_template('show_category.html', form=form, title=title, menu=menu, data=data, key=keyword_for_request)
    return render_template('category.html', form=form, title=title, menu=menu)


@app.route('/deal', methods=['GET', 'POST'])
def deal():
    title = 'Add new activity'
    method = request.method
    form = SelectForm2()
    form.deal_type.choices = reading_from_type(app.config.get('DB_URL'))
    if method == "POST":
        keyword_for_request = request.form['deal_type']
        return redirect(url_for("activity_adding", data=keyword_for_request))
    return render_template('deal.html', form=form, title=title, menu=menu)


@app.route('/activity_adding')
def activity_adding():
    title = 'New activity added in table'
    temp_dict = {'type': {request.args.get('data')}}
    data_from_api = random_deal_request(temp_dict)[0]
    edit_tdl_table(app.config.get('DB_URL'), data_from_api)
    return render_template('activity_adding.html', data=data_from_api, title=title, menu=menu)


@app.route('/new_type', methods=['GET', 'POST'])
def new_type():
    title = 'Add new type of activity'
    method = request.method
    form = SelectForm()
    list_of_types = reading_from_type(app.config.get('DB_URL'))
    if method == "POST":
        search_word = request.form['category']
        edit_type_table(app.config.get('DB_URL'), list_of_types, search_word)
        return redirect('/done')
    return render_template('new_type.html', form=form, title=title, menu=menu)


@app.route('/done')
def done():
    title = 'New activity type added to the table'
    data = reading_from_type(app.config.get('DB_URL'))
    return render_template('done.html', data=data, title=title, menu=menu)
