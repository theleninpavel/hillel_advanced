from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import Post, Category


admin.site.register(Category)


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('text', 'headline')}
