from django.contrib.auth.models import User
from django.db import models


# Create your models here.
from django.utils import timezone


class Category(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return f'{self.name}(id:{self.pk})'


class Post(models.Model):
    category = models.ManyToManyField(Category)
    headline = models.CharField(max_length=255)
    writer = models.ForeignKey(User, null=True, blank=True, default=None, on_delete=models.CASCADE)
    text = models.TextField()
    publish = models.DateTimeField(default=timezone.now)
    create = models.DateTimeField(auto_now_add=True)
    edited = models.DateTimeField(auto_now=True)
    slug = models.SlugField(unique=True, allow_unicode=True)

    def __str__(self):
        return f'{self.pk}: {self.headline}'

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.slug:
            self.slug = f'{str(self.headline).lower().replace(" ", "-").replace("/", "-")[:20]}'\
                        + f'{str(self.text).lower().replace(" ", "-").replace("/", "-")[:5]}'
        super().save(force_insert, force_update, using, update_fields)

    def get_absolute_url(self):
        from django.urls import reverse
        # return reverse('blog:certain_post', kwargs={'post_id': self.slug})
        return reverse('blog:item_view', kwargs={'post_id': self.slug})
