from django.urls import path

from . import views
from .views import BlogsView, CategoryView, PostUpdateView, PostDetailView, PostCreateView

app_name = "blog"

urlpatterns = [
    # path('', views.main, name='main'),
    path('', BlogsView.as_view(), name='view_all'),
    # path('post/<post_id>/', views.certain_post, name='certain_post'),
    # path('item_view/<post_id>/', ItemView.as_view(), name='item_view'),
    path('item/<post_id>/', PostDetailView.as_view(), name='detail_view'),
    # path('add/', views.add, name='add_post'),
    path('view_add/', PostCreateView.as_view(), name='view_add'),
    path('add_category/', views.add_category, name='add_category'),
    path('filter/<int:cat_id>/', views.filter_by_category, name='filter_by_category'),
    path('filter_user/<user_id>/', views.filter_by_user, name='filter_by_user'),
    # path('all_category/', views.all_category, name='all_category'),
    path('all_category_view/', CategoryView.as_view(), name='all_category_view'),
    path('edit/<post_id>/', PostUpdateView.as_view(), name='post_update'),
]