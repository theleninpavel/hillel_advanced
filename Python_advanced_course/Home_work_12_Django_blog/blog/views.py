from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.views import View
from django.views.generic import ListView, UpdateView, DetailView, CreateView
# Create your views here.


from .forms import PostForm, CategoryForm
from .models import Post, Category


# def main(request): # Function returns main page
#     titles = Post.objects.all()
#     return render(request,
#                   'blog/main.html',
#                   context={'titles': titles, 'header': 'Main page'})


# def certain_post(request, post_id: int):  # Function returns one post by slug
#     title = Post.objects.get(slug=post_id)
#     category = title.category.all()
#     return render(request, 'blog/certain_post.html',
#                   context={'title': title, 'category': category, 'header': 'Certain post'})


# def all_category(request):  # Function to display all categories
#     all_cat = Category.objects.all()
#     return render(request, 'blog/all_category.html',
#                   context={'all_cat': all_cat, 'header': 'All category'})


def filter_by_category(request, cat_id: int):  # Function that displays posts filtered by category name
    category = Category.objects.get(pk=cat_id)
    titles = Post.objects.filter(category=category.pk)
    return render(request, "blog/filter_by_category.html",
                  context={'titles': titles, 'category': category, 'header': 'Filter by caegory'})


def filter_by_user(request, user_id: int):  # Function that displays posts filtered by category name
    user = User.objects.get(pk=user_id)
    titles = Post.objects.filter(writer=user.pk)
    return render(request, "blog/filter_by_user.html",
                  context={'titles': titles, 'header': 'Filter by user'})


# @login_required
# def add(request):  # Function to add a new post
#     # if request.user in User.objects.all():
#     if request.method == 'POST':
#         form = PostForm(data=request.POST)
#         if form.is_valid():
#             form_name = request.POST.get('headline')
#             if Post.objects.all().filter(headline=form_name):
#                 error = 'Headline already exist'
#                 return render(request, 'blog/add_post.html',
#                               context={'form': form, 'error': error, 'header': 'Add new category'})
#             new_post = form.save(commit=False)
#             new_post.writer = request.user
#             new_post = form.save()
#             return redirect('blog:certain_post', post_id=new_post.slug)
#         return render(request, 'blog/add_post.html', context={'form': form, 'header': 'Add post'})
#     form = PostForm
#     return render(request, 'blog/add_post.html', context={'form': form, 'header': 'Add post'})
#     # return redirect('blog:main')

@staff_member_required(redirect_field_name='blog/main.html')
def add_category(request):  # Function to add a new category
    if request.method == 'POST':
        form = CategoryForm(data=request.POST)
        if form.is_valid():
            form_name = request.POST.get('name')
            if Category.objects.all().filter(name=form_name):
                error = 'Category already exist'
                return render(request, 'blog/add_category.html',
                              context={'form': form, 'error': error, 'header': 'Add new category'})
            form.save()
            return redirect('blog:all_category_view')
        return render(request, 'blog/add_category.html',
                      context={'form': form, 'header': 'Add new category'})
    form = CategoryForm
    return render(request, 'blog/add_category.html',
                  context={'form': form, 'header': 'Add new category'})


# class ItemView(View):
#     def get(self, request, post_id):
#         title = Post.objects.get(slug=post_id)
#         category = title.category.all()
#         return render(request, 'blog/certain_post.html',
#                       context={'title': title, 'category': category, 'header': 'Certain post'})


class PostCreateView(CreateView):
    model = Post
    form_class = PostForm
    template_name = 'blog/view_add_post.html'

    def form_valid(self, form):
        post_name = self.request.POST.get('name')
        if Post.objects.all().filter(headline=post_name):
            error = 'Name already exist'
            return render(self.request, 'blog/view_add_post.html', context={'form': form, 'error': error})

        new_post = form.save(commit=False)
        new_post.writer = self.request.user
        new_post.save()
        form.save_m2m()
        return super().form_valid(form)


class BlogsView(ListView):
    model = Post
    context_object_name = 'titles'
    template_name = 'blog/view_all.html'


class PostDetailView(DetailView):
    model = Post
    context_object_name = 'title'
    template_name = 'blog/post_detail.html'
    slug_url_kwarg = 'post_id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        if self.object:
            context['categories'] = self.object.category.all()
            context['author'] = self.object.writer
        return context


class PostUpdateView(UpdateView):
    model = Post
    slug_url_kwarg = 'post_id'
    fields = ['headline', 'category', 'text']
    template_name = 'blog/post_edit.html'


class CategoryView(ListView):
    model = Category
    context_object_name = "all_cat"
    template_name = 'blog/all_category_view.html'
