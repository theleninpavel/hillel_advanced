from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.shortcuts import render, redirect


# Create your views here.
from .forms import ExtendedUserCreationForm


def login_view(request):
    if request.POST:
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            login(request, form.get_user())
            print(f'Form_get_user: {form.get_user()}')
            # return redirect('blog:main')
            return redirect('blog:view_all')
        return render(request, 'user/login.html', context={'form': form})
    form = AuthenticationForm
    return render(request, 'user/login.html', context={'form': form})


def logout_view(request):
    logout(request)
    # return redirect('blog:main')
    return redirect('blog:view_all')


def signup_view(request):
    if request.method == 'POST':
        form = ExtendedUserCreationForm(data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('user:login')
        print(type(form.errors))
        return render(request, 'user/signup.html', context={'form': form})
    form = ExtendedUserCreationForm
    return render(request, 'user/signup.html', context={'form': form})
