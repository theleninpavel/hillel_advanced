from typing import List


def my_range(start, stop, step=1) -> List[int]:
    """
    Simply function. Return a value of variable in range between 'start' and 'stop'
    with a given step(by default step = 1)
    :type start: int
    :param start: int
    :param stop: int
    :param step: int
    :return: int
    """
    while start < stop:
        yield start
        start += step


f = my_range(-5, 10)
for i in f:
    print(f'{i}')
print()

f = my_range(-5, 10, 2)
for i in f:
    print(f'{i}')
print()

f = my_range(-5, 100, 5)
for i in f:
    if i == 0:
        continue
    print(f'{i}')

print()
