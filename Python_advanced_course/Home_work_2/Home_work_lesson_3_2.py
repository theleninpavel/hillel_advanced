from typing import List


def main_func(func, items) -> List[float]:
    for item in items:
        yield func(item)


def inches_to_mm(inches: float) -> float:
    """
    Converting inches to millimeters
    :param inches: float
    :return: float
    """
    return inches * 25.4


f3 = main_func(inches_to_mm, [1/2, 3/4, 1, 1.25, 1.5, 2, 4])

for i in f3:
    print(f'{i:.1f} mm')
