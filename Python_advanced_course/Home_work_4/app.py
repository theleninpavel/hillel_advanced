from typing import List
from flask import Flask, render_template
import builtins

applic = Flask("MySite")


def main_reader() -> List:
    with open('text.txt') as data_from_file:
        lines = data_from_file.readlines()
        return lines


data = main_reader()
main_title = '"Song of Myself(1892 version)" BY WALT WHITMAN'


@applic.route('/')
def main_page(main_title=main_title):
    return render_template('First_page.html', main_title=main_title, line=data, **builtins.__dict__)


@applic.route('/filter/<path:word>')
def filter(word):
    return render_template('printing_res.html', main_title=main_title, word=word, line=data, **builtins.__dict__)


@applic.route('/strings/<int:numb_strings>')
def first_n_strings(numb_strings):
    list_of_strings = []
    for i in range(numb_strings):
        list_of_strings.append(data[i])
    return render_template('first_n_strings.html', number=numb_strings,
                           list_of_strings=list_of_strings, line=data, **builtins.__dict__)


if __name__ == '__main__':
    applic.run()
