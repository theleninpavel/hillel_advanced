from typing import Union


class Calculator:
    def add(self, val_1: Union[int, float], val_2: Union[int, float]):
        pass


class IntegerCalculator(Calculator):
    def add(self, val_1: Union[int, float], val_2: Union[int, float]) -> int:
        return int(val_1 + val_2)


class FloatCalculator(Calculator):
    def add(self, val_1: Union[int, float], val_2: Union[int, float]) -> float:
        return float(val_1 + val_2)


def make_add(obj, arg1: Union[int, float], arg2: Union[int, float]) -> Union[int, float]:
    return obj.add(arg1, arg2)


int_calc = IntegerCalculator()
result = make_add(int_calc, 1.0, 2.0)
print(f"RESULT:  {result}")
int_calc = FloatCalculator()
result = make_add(int_calc, 1, 2)
print(f"RESULT:  {result}")
