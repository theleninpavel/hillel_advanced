from functools import wraps
from typing import Tuple, Any


def decorator(function):
    @wraps(function)
    def inner_function(*args, **kwargs):
        named_args = ', '.join([f'{k}={v}' for k, v in kwargs.items()])
        pos_args = ', '.join(map(str, args))
        return(f'Function name: "{function.__name__}"\n'
               f'Function positional arguments: {pos_args}\n'
               f'Function named arguments: {named_args}')
    return inner_function



@decorator
def some_function_with_long_long_name(*args: Any, **kwargs: Any) -> Tuple:
    return args, kwargs


some_dict = {"a": 89}
random_var = 44
print(some_function_with_long_long_name(random_var, 66, 'c', [5, 8], some_dict, y=8, k=8, j=7))
