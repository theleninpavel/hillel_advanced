from typing import List
from flask import Flask, render_template
from helpers import reader_from_database, searching_in_category, searching_in_buyer
import builtins

applic = Flask("MySite")


@applic.route('/')
def main_page():
    data = reader_from_database()
    return render_template("all.html", data=data)


@applic.route('/category/<path:word>')
def filtration_by_category(word):
    data = searching_in_category(word)
    return render_template('printing_res.html', data=data, word=word)


@applic.route('/buyer/<path:word>')
def filtration_by_buyer(word):
    data = searching_in_buyer(word)
    return render_template('printing_res.html', data=data, word=word)


if __name__ == '__main__':
    applic.run()
