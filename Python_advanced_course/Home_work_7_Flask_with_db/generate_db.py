import json
import sqlite3
from typing import List, Dict, NoReturn


def reader_from_json() -> List[Dict]:
    with open("shopping_list.json") as f:
        purchases = json.load(f)
    return purchases


def creating_database() -> NoReturn:
    with sqlite3.connect('shopping_database.db') as db_connection:
        cursor = db_connection.cursor()
        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS purchases
            (
            purchase_id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            category TEXT NOT NULL,
            amount INTEGER DEFAULT 0,
            price REAL DEFAULT 0,
            buyer TEXT NOT NULL
            )
            """)
        cursor.executemany("""
            INSERT INTO
                purchases(name, category, amount, price, buyer)
            VALUES
                (:name,:category, :amount, :price, :buyer)
        """, reader_from_json())
        cursor.close()
        db_connection.commit()


if __name__ == "__main__":
    creating_database()
