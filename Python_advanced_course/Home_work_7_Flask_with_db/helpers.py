import sqlite3
from typing import List


def reader_from_database() -> List:
    with sqlite3.connect('shopping_database.db') as db_connection:
        db_connection.row_factory = sqlite3.Row
        cursor = db_connection.cursor()
        purchases = cursor.execute("""
            SELECT * FROM purchases
        """)
        items = []
        for item in purchases:
            items.append(dict(item))
        return items


def searching_in_category(prmtr: str) -> List:
    with sqlite3.connect('shopping_database.db') as db_connection:
        db_connection.row_factory = sqlite3.Row
        cursor = db_connection.cursor()
        purchases = cursor.execute(f"""
            SELECT * 
            FROM purchases WHERE category LIKE '%{prmtr}%'
            """)
        items = []
        for item in purchases:
            items.append(dict(item))
        return items


def searching_in_buyer(prmtr: str) -> List:
    with sqlite3.connect('shopping_database.db') as db_connection:
        db_connection.row_factory = sqlite3.Row
        cursor = db_connection.cursor()
        purchases = cursor.execute(f"""
            SELECT * 
            FROM purchases WHERE buyer LIKE '%{prmtr}%'
            """)
        items = []
        for item in purchases:
            items.append(dict(item))
        return items
