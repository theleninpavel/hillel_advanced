
class Person:
    def __init__(self, idn: str, name: str, gender: str, height: str, weight=None) -> None:
        self.idn = idn
        self.name = name
        self.gender = gender
        self.height = height
        self.weight = weight

    def __str__(self) -> str:
        return f'{self.idn}; {self.name}; {self.gender}; ' \
               f'{self.height} cm; {self.weight} kg.'
