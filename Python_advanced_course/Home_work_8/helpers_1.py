import csv
from typing import List, Dict

from class_description import Person


willsmith = {
    'idn': '123456',
    'name': 'Robert Neville',
    'gender': 'male',
    'height': '188',
    'weight': '95'
}
tomhanks = {
    'idn': '234567',
    'name': 'Forest Gump',
    'gender': 'male',
    'height': '183',
    'weight': '73'
    }
wesleysnipes = {
    'idn': '345678',
    'name': 'Simon Phoenix',
    'gender': 'male',
    'height': '175',
    'weight': '88'
    }
arnoldschwarzenegger = {
    'idn': '456789',
    'name': 'Douglas Quaid',
    'gender': 'male',
    'height': '188',
    'weight': '95'
    }
michellepfeiffer = {
    'idn': '567890',
    'name': 'Selina Kyle',
    'gender': 'female',
    'height': '171',
    'weight': '60'
    }
people = [
    willsmith,
    wesleysnipes,
    tomhanks,
    arnoldschwarzenegger,
    michellepfeiffer
    ]


def making_objects(prmtr: List) -> List:
    temp_list = []
    for i in prmtr:
        temp_list.append(Person(**i))
    return temp_list


def reader() -> List[Dict]:
    with open('names.csv', newline='') as csvfile:
        readerin = csv.DictReader(csvfile)
        list_of_dicts = [item for item in readerin]
        return list_of_dicts


if __name__ == "__main__":
    for item in making_objects(reader()):
        print(item)



