import csv
from typing import List

from helpers_1 import making_objects, people
from class_description import Person


def class_to_dict_list(prmtr: List[Person]) -> List:
    temp_list = []
    pattern_dict = {'idn': None, 'name': None, 'gender': None, 'height': None, 'weight': None}
    for i in prmtr:
        # print(f'I={i}; {type(i)}')
        temp_dict = {}
        for k in pattern_dict.keys():
            temp_dict.update({k: Person.__getattribute__(i, k)})
            #print(f'<<{Person.__getattribute__(i, k)}; i:{i}, k:{k}>>')
        temp_list.append(temp_dict)
        # print(f"LAST:{Person.__getattribute__(i, 'idn')}...{Person.__getattribute__(i, 'name')}"
        #       f"...{Person.__getattribute__(i, 'gender')}")
    return temp_list


def writer(prmtr: List):
    with open('names.csv', 'wt') as csvfile:
        fieldnames = ['idn', 'name', 'gender', 'height', 'weight']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for i in prmtr:
            writer.writerow(i)


if __name__ == "__main__":
    func1 = making_objects(people)
    func2 = class_to_dict_list(func1)
    writer(func2)

