

class Person:
    def __init__(self, name: str, second_name: str, gender: str, salary=None,
                 profession_id=None, email=None, age=0) -> None:
        self.name = name
        self.second_name = second_name
        self.gender = gender
        self.salary = salary
        self.profession_id = profession_id
        self.email = email
        self.age = age

    def __str__(self) -> str:
        return f'name: {self.name}; second name: {self.second_name}; gender: {self.gender}; ' \
               f'salary: {self.salary}; profession: {self.profession_id}; email: {self.email}; ' \
               f'age: {self.age}'
