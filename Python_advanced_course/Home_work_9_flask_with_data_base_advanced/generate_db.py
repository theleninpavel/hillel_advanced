import sqlite3
from typing import NoReturn


def creating_database() -> NoReturn:
    with sqlite3.connect('people_hw_9.db') as db_connection:
        cursor = db_connection.cursor()
        cursor.execute(
            """
            DROP TABLE IF EXISTS professions;
            """)
        cursor.execute(
            """
            DROP TABLE IF EXISTS genders;
            """)
        cursor.execute(
            """
            DROP TABLE IF EXISTS people;
            """)
        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS professions
            (
                profession_id INTEGER PRIMARY KEY AUTOINCREMENT,
                name TEXT UNIQUE NOT NULL
            )
            """)
        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS genders
            (
                gender_id INTEGER PRIMARY KEY AUTOINCREMENT,
                name VARCHAR(15)
            )
            """)
        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS people
            (
                people_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                name TEXT NOT NULL,
                second_name TEXT NOT NULL,
                gender INTEGER NOT NULL,
                salary INTEGER,
                profession_id INTEGER,
                email VARCHAR(30),
                age INTEGER NOT NULL,
                FOREIGN KEY (gender)
                    REFERENCES genders (gender_id),
                FOREIGN KEY (profession_id)
                    REFERENCES professions (profession_id)
            )
            """)
        cursor.execute("""
            INSERT INTO
               genders(name)
            VALUES
               ('male'),
               ('female'),
               ('not specified')
            """)
        cursor.execute("""
            INSERT INTO
               professions(name)
            VALUES
               ('director'),
               ('writer'),
               ('poet'),
               ('scientist'),
               ('engineer'),
               ('singer'),
               ('military'),
               ('politician'),
               ('astronaut'),
               ('not specified')
            """)
        cursor.close()
        db_connection.commit()