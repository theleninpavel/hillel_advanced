import json
import random
import sqlite3
from sqlite3 import Cursor
from typing import List, Dict, NoReturn

from class_description import Person


def creating_person_object(prmtr: List) -> List[Dict]:
    temp_list = []
    for i in prmtr:
        temp_list.append(Person(**i))
    temp_list_2 = []
    pattern_dict = {'name': None, 'second_name': None, 'gender': None, 'salary': None,
                    'profession_id': None, 'email': None, 'age': None}
    for i in temp_list:
        temp_dict = {}
        for k in pattern_dict.keys():
            temp_dict.update({k: Person.__getattribute__(i, k)})
        for key in temp_dict.keys():
            if key == 'age':
                temp_dict['age'] = random.randint(25, 90)
        if temp_dict['profession_id'] is None:
            temp_dict['salary'] = 0
        else:
            temp_dict['salary'] = random.randint(1000, 100000)
        temp_list_2.append(temp_dict)
    return temp_list_2


def reader_from_json() -> List[Dict]:
    with open("list_of_people.json") as f:
        list_of_dicts = json.load(f)
    return list_of_dicts


def editing_people_db(prmtr: List[Dict]) -> NoReturn:
    with sqlite3.connect('people_hw_9.db') as db_connection:
        cursor = db_connection.cursor()
        cursor.execute("""
            PRAGMA foreign_keys = ON;
            """)
        cursor.executemany("""
            INSERT INTO
                people(name, second_name, gender, salary, profession_id, email, age)
            VALUES
                (:name, :second_name, :gender, :salary, :profession_id, :email, :age)
            """, prmtr)
    cursor.close()
    db_connection.commit()


def editing_gender_db(prmtr: Dict) -> NoReturn:
    with sqlite3.connect('people_hw_9.db') as db_connection:
        cursor = db_connection.cursor()
        cursor.execute("""
            PRAGMA foreign_keys = ON;
            """)
        cursor.execute(f"""
            INSERT INTO
               genders(name)
            VALUES
               (:name)
            """, prmtr)
    cursor.close()
    db_connection.commit()


def manual_editing_db(prmtr: Dict):
    temp_list = list(prmtr.items())
    with sqlite3.connect("people_hw_9.db") as db_connection:
        cursor = db_connection.cursor()
        cursor.execute("""
            PRAGMA foreign_keys = ON;
        """)
        table_id = cursor.execute(f"""
            SELECT people_id 
            FROM people 
            WHERE name LIKE '{temp_list[0][1]}' AND second_name LIKE '{temp_list[1][1]}'
        """)
        for item in table_id:
            people_id = item[0]
            cursor.execute(f"""
                UPDATE people
                SET {temp_list[2][0]} = '{temp_list[2][1]}'
                WHERE people_id = {people_id}
            """)
    cursor.close()
    db_connection.commit()


def getting_query_result(prmtr: Dict) -> Cursor:
    temp_list = list(prmtr.items())
    with sqlite3.connect("people_hw_9.db") as db_connection:
        cursor = db_connection.cursor()
        result = cursor.execute(f"""
            SELECT name, second_name
            FROM people p 
            WHERE {temp_list[0][0]} LIKE '{temp_list[0][1]}'
        """)
        return result


def getting_email_result() -> NoReturn:
    with sqlite3.connect("people_hw_9.db") as db_connection:
        db_connection.row_factory = sqlite3.Row
        cursor = db_connection.cursor()
        result = cursor.execute("""
            SELECT  name, second_name, name || second_name || '@umbrella.corp' as email
            FROM people WHERE email IS NULL   
        """)
        temp_list = []
        for item in result:
            temp_list.append(dict(item))
        return temp_list



