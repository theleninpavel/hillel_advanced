
from Python_advanced_course.Home_work_9_flask_with_data_base_advanced.generate_db import creating_database
from Python_advanced_course.Home_work_9_flask_with_data_base_advanced.helpers import editing_people_db, \
    creating_person_object, reader_from_json, manual_editing_db, editing_gender_db, getting_query_result, \
    getting_email_result


if __name__ == "__main__":
    creating_database()
    editing_people_db(creating_person_object(reader_from_json()))
    x = input('For adding Laurence and Andrew Wachowski, press Enter key')
    data = [{'name': 'Laurence', 'second_name': 'Wachowski', 'gender': 1, 'profession_id': 1},
            {'name': 'Andrew', 'second_name': 'Wachowski', 'gender': 1, 'profession_id': 1}]
    editing_people_db(creating_person_object(data))
    print()
    l = input('For changing Laurence and Andrew Wachowski gender, press Enter key')
    input_data = {'old_name': 'Andrew', 'second_name': 'Wachowski', 'gender': 2}
    manual_editing_db(input_data)
    input_data = {'old_name': 'Laurence', 'second_name': 'Wachowski', 'gender': '2'}
    manual_editing_db(input_data)
    print()
    l = input('For changing Laurence and Andrew Wachowski names, press Enter key')
    input_data = {'old_name': 'Andrew', 'second_name': 'Wachowski', 'name': 'Lilly'}
    manual_editing_db(input_data)
    input_data = {'old_name': 'Laurence', 'second_name': 'Wachowski', 'name': 'Lana'}
    manual_editing_db(input_data)
    print()
    l = input('For adding new gender press any key')
    new_gender = {'name': 'foam rubber'}
    editing_gender_db(new_gender)
    print()
    l = input('For changing SpongeBob gender, press Enter key')
    manual_editing_db({'name': "SpongeBob", 'second_name': 'SquarePants', 'gender': '4'})
    print()
    l = input('For printing people with new gender press any key')
    data = {'gender': 4}
    for item in getting_query_result(data):
        print(f'Result: {item[0]}  {item[1]}')
    print()
    l = input('For printing people with profession of a scientist, press Enter key')
    data = {'profession_id': 4}
    for item in getting_query_result(data):
        print(f'Result: {item[0]}  {item[1]}')
    print()
    l = input('For add missing email press Enter key')
    for item in getting_email_result():
        manual_editing_db(item)
